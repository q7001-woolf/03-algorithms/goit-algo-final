def greedy_algorithm(items, budget):
    # Сортуємо страви за співвідношенням калорій до вартості в порядку спадання
    sorted_items = sorted(items.items(), key=lambda x: x[1]['calories'] / x[1]['cost'], reverse=True)
    
    chosen_items = {}
    total_cost = 0
    total_calories = 0
    
    # Вибираємо страви, доки не вичерпаємо бюджет
    for item, info in sorted_items:
        while total_cost + info['cost'] <= budget:
            total_cost += info['cost']
            total_calories += info['calories']
            chosen_items[item] = chosen_items.get(item, 0) + 1
    
    return chosen_items, total_calories

from collections import Counter

def dynamic_programming(items, budget):
    # Create a table to store the maximum calories for each budget
    dp = [0 for _ in range(budget + 1)]
    chosen_items = [[] for _ in range(budget + 1)]
    
    for item, info in items.items():
        for b in range(info['cost'], budget + 1):
            if dp[b] < dp[b - info['cost']] + info['calories']:
                dp[b] = dp[b - info['cost']] + info['calories']
                chosen_items[b] = chosen_items[b - info['cost']] + [item]
    
    # Count the occurrences of each item in the chosen items list
    chosen_items_count = Counter(chosen_items[budget])
    
    return chosen_items_count, dp[budget]

items = { "pizza": {"cost": 50, "calories": 300}, "hamburger": {"cost": 40, "calories": 250}, "hot-dog": {"cost": 30, "calories": 200}, "pepsi": {"cost": 10, "calories": 100}, "cola": {"cost": 15, "calories": 220}, "potato": {"cost": 25, "calories": 350} }

budget = 100
print("Greedy Algorithm:")
print(greedy_algorithm(items, budget))

print("Dynamic Programming:")
print(dynamic_programming(items, budget))