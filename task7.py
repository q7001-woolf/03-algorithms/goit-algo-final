import random
import pandas as pd


def simulate_dice_rolls(n):
    sums = {i: 0 for i in range(2, 13)}
    for _ in range(n):
        roll1 = random.randint(1, 6)
        roll2 = random.randint(1, 6)
        sums[roll1 + roll2] += 1
    probabilities = {i: sums[i] / n for i in sums}
    return probabilities

def print_probabilities(probabilities):
    df = pd.DataFrame(list(probabilities.items()), columns=['Sum', 'Probability'])
    print(df)

n = 100000
probabilities = simulate_dice_rolls(n)
print_probabilities(probabilities)

print("Метод Монте - Карло доволі точно оцінює ймовірності, коли кількість спроб зростає. У цьому випадку, коли n = 100000, результати наближені до теоретичниних значень.")