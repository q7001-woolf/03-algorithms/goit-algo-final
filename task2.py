import turtle

def draw_tree(t, branch_len):
    angle = 30
    sf = 0.8  # scale factor for branch length

    if branch_len < 3:
        return
    else:
        t.forward(branch_len)
        t.right(angle)
        draw_tree(t, branch_len * sf)

        t.left(2 * angle)
        draw_tree(t, branch_len * sf)

        t.right(angle)
        t.backward(branch_len)

recursion_level = int(input("Enter the recursion level: "))

t = turtle.Turtle()
t.speed(0)  # fastest speed
t.left(90)  # make the turtle face upwards
t.up()
t.backward(200)  # move the turtle to a position
t.down()

draw_tree(t, recursion_level * 10)

turtle.mainloop()